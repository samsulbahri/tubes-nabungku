package com.example.mytubes.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.mytubes.fragment.PemasukanFragment;
import com.example.mytubes.fragment.PengeluaranFragment;
import com.example.mytubes.R;
import com.example.mytubes.fragment.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity  {

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.navigation);

        if(getIntent().getBooleanExtra("KEY_IS_FROM_PENGELUARAN", false)) {
            PengeluaranFragment fragment = new PengeluaranFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, fragment);
            fragmentTransaction.commit();
            setTitle("Pengeluaran");

            bottomNavigationView.setSelectedItemId(R.id.navigation_pengeluaran);
        } else {
            PemasukanFragment mainFragment = new PemasukanFragment();
            FragmentTransaction mainFragmentTransaction = getSupportFragmentManager().beginTransaction();
            mainFragmentTransaction.replace(R.id.frame_layout, mainFragment);
            mainFragmentTransaction.commit();
            setTitle("Pemasukan");
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            int id = menuItem.getItemId();

            if(id == R.id.navigation_pemasukan) {
                PemasukanFragment fragment = new PemasukanFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.commit();
                setTitle("Pemasukan");
            }
            else if (id == R.id.navigation_pengeluaran){
                PengeluaranFragment fragment = new PengeluaranFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.commit();
                setTitle("Pengeluaran");
            } else if(id == R.id.navigation_profile) {
                ProfileFragment fragment = new ProfileFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.commit();
                setTitle("Profile");
            }
            return true;
        });
    }
}