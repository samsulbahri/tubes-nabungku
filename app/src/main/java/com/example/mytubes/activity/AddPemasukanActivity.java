package com.example.mytubes.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mytubes.R;
import com.example.mytubes.entity.Saldo;
import com.example.mytubes.helper.SqliteHelper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddPemasukanActivity extends AppCompatActivity {

    private EditText edSaldo, edCatatan;
    private Button btnAdd;

    private String jumlahSaldo;

    private SqliteHelper sqliteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pemasukan);

        sqliteHelper = new SqliteHelper(this);

        edSaldo = findViewById(R.id.et_saldo_tambah_pms);
        edCatatan = findViewById(R.id.et_catatan_tambah_pms);
        btnAdd = findViewById(R.id.bt_tambah_pms);

        edSaldo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edSaldo.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();
                    originalString = originalString.replaceAll("\\.", "").replaceFirst(",", ".");
                    originalString = originalString.replaceAll("[A-Z]", "").replaceAll("[a-z]", "");
                    int doubleval = Integer.parseInt(originalString);
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setDecimalSeparator(',');
                    symbols.setGroupingSeparator('.');
                    String pattern = "#,###,###";
                    DecimalFormat formatter = new DecimalFormat(pattern, symbols);
                    String formattedString = formatter.format(doubleval);
                    jumlahSaldo = formattedString.replace(".","");
                    edSaldo.setText(formattedString);
                    edSaldo.setSelection(edSaldo.getText().length());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                edSaldo.addTextChangedListener(this);

                edSaldo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnAdd.setOnClickListener(v -> {
            if(edSaldo.getText().toString().matches("") || edCatatan.getText().toString().matches("")) {
                Toast.makeText(this, "Pastikan tidak ada data yang kosong!", Toast.LENGTH_SHORT).show();
            } else {
                Saldo saldo = new Saldo();

                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                String date = formatter.format(new Date());

                saldo.setSaldo(jumlahSaldo);
                saldo.setCatatan(edCatatan.getText().toString());
                saldo.setTanggal(date);

                boolean isInsert = sqliteHelper.addPemasukan(saldo);

                if(isInsert) {
                    startActivity(new Intent(this, MainActivity.class));
                } else {
                    Toast.makeText(this, "Data Tidak Masuk!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}