package com.example.mytubes.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mytubes.R;
import com.example.mytubes.helper.MethodFunction;
import com.example.mytubes.helper.SqliteHelper;

import java.util.Calendar;

public class ProfileFragment extends Fragment {

    TextView greetings, pemasukan, pengeluaran, totalSaldo;

    private SqliteHelper sqliteHelper;

    MethodFunction methodFunction = new MethodFunction();

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sqliteHelper = new SqliteHelper(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        greetings = view.findViewById(R.id.tv_profil_greetings);
        pemasukan = view.findViewById(R.id.tv_pemasukan_total);
        pengeluaran = view.findViewById(R.id.tv_pengeluaran_total);
        totalSaldo = view.findViewById(R.id.tv_profile_saldo);

        return view;
    }

    @Override
    public  void onViewCreated(View view, Bundle savedIntanceState) {
        Calendar calendar = Calendar.getInstance();
        int jam = calendar.get(Calendar.HOUR_OF_DAY);

        if(jam >= 0 && jam < 4) {
            greetings.setText(R.string.cant_sleep);
        } else if(jam >= 4 && jam < 12) {
            greetings.setText(R.string.greeting_morning);
        } else if(jam >= 12 && jam < 15) {
            greetings.setText(R.string.greeting_afternoon);
        } else if(jam >= 15 && jam < 19) {
            greetings.setText(R.string.greeting_afternoon_2);
        } else if(jam >= 19 && jam < 24) {
            greetings.setText(R.string.greeting_evening);
        }

        pemasukan.setText(methodFunction.currencyIdr(sqliteHelper.getSumPemasukan()));

        pengeluaran.setText(methodFunction.currencyIdr(sqliteHelper.getSumPengeluaran()));

        totalSaldo.setText(methodFunction.currencyIdr(sqliteHelper.getTotalUang()));
    }
}