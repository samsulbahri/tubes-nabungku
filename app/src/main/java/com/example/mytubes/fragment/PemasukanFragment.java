package com.example.mytubes.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mytubes.activity.AddPemasukanActivity;
import com.example.mytubes.adapter.PemasukanAdapter;
import com.example.mytubes.helper.MethodFunction;
import com.example.mytubes.entity.Saldo;
import com.example.mytubes.R;
import com.example.mytubes.helper.SqliteHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class PemasukanFragment extends Fragment {

    MethodFunction methodFunction = new MethodFunction();

    private SqliteHelper sqliteHelper;

    private TextView textNotFound, textSum;

    private RecyclerView rvPemasukan;

    private FloatingActionButton addButton;

    private PemasukanAdapter adapter;

    public PemasukanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sqliteHelper = new SqliteHelper(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pemasukan, container, false);

        textNotFound = view.findViewById(R.id.text_tidak_ditemukan_pemasukan);
        textSum = view.findViewById(R.id.tv_pemasukan_total);
        rvPemasukan = view.findViewById(R.id.rv_pemasukan);
        addButton = view.findViewById(R.id.bt_tambah_pemasukan);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        List<Saldo> saldos = sqliteHelper.getAllPemasukan();

        if(saldos.size() > 0) {
            textNotFound.setVisibility(View.INVISIBLE);

            rvPemasukan.setLayoutManager(new LinearLayoutManager(getContext()));

            adapter = new PemasukanAdapter(saldos);
            
            rvPemasukan.setAdapter(adapter);

            ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull  RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    final int position = viewHolder.getAbsoluteAdapterPosition();
                    final Saldo saldo = adapter.getData().get(position);

                    adapter.removeItem(position);

                    final boolean[] isDeleted = {true};

                    Snackbar snackbar = Snackbar.make(viewHolder.itemView, "Data anda berhasil dihapus.", Snackbar.LENGTH_LONG);
                    snackbar.setAction("BATAL", v -> {
                        isDeleted[0] = false;

                        adapter.restoreItem(saldo, position);

                        int sum = 0;
                        for (Saldo item : adapter.getData()) {
                            sum += Integer.parseInt(item.getSaldo());
                        }

                        textSum.setText(methodFunction.currencyIdr(sum));

                        sqliteHelper.addPemasukan(saldo);
                    });

                    snackbar.show();

                    if(isDeleted[0]) {
                        sqliteHelper.deletePemasukan(saldo.getDataID());

                        int sum = 0;
                        for (Saldo item : adapter.getData()) {
                            sum += Integer.parseInt(item.getSaldo());
                        }

                        textSum.setText(methodFunction.currencyIdr(sum));
                    }
                }
            };

            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
            itemTouchHelper.attachToRecyclerView(rvPemasukan);

            int sum = 0;
            for (Saldo saldo : saldos) {
                sum += Integer.parseInt(saldo.getSaldo());
            }

            textSum.setText(methodFunction.currencyIdr(sum));

        } else {
            textNotFound.setVisibility(View.VISIBLE);
            rvPemasukan.setVisibility(View.GONE);
        }

        addButton.setOnClickListener(it -> {
            it.getContext().startActivity(new Intent(it.getContext(), AddPemasukanActivity.class));
        });
    }

}