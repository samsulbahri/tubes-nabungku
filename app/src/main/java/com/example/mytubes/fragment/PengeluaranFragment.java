package com.example.mytubes.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mytubes.R;
import com.example.mytubes.activity.AddPengeluaranActivity;
import com.example.mytubes.adapter.PengeluaranAdapter;
import com.example.mytubes.entity.Saldo;
import com.example.mytubes.helper.MethodFunction;
import com.example.mytubes.helper.SqliteHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class PengeluaranFragment extends Fragment {

    MethodFunction methodFunction = new MethodFunction();

    private SqliteHelper sqliteHelper;

    private TextView textNotFound, textSum;

    private RecyclerView rvPengeluaran;

    private FloatingActionButton addButton;

    private PengeluaranAdapter adapter;
    public PengeluaranFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sqliteHelper = new SqliteHelper(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pengeluaran, container, false);
        textNotFound = view.findViewById(R.id.text_tidak_ditemukan_pengeluaran);
        textSum = view.findViewById(R.id.tv_pengeluaran_total);
        rvPengeluaran = view.findViewById(R.id.rv_pengeluaran);
        addButton = view.findViewById(R.id.bt_tambah_pengeluaran);

        return view;
    }

    @Override
    public  void onViewCreated(View view, Bundle savedIntanceState){
        List<Saldo> saldos = sqliteHelper.getAllPengeluaran();

        if(saldos.size() > 0){
            textNotFound.setVisibility(View.INVISIBLE);

            rvPengeluaran.setLayoutManager(new LinearLayoutManager(getContext()));

            adapter = new PengeluaranAdapter(saldos);

            rvPengeluaran.setAdapter(adapter);

            ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull  RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    final int position = viewHolder.getAbsoluteAdapterPosition();
                    final Saldo saldo = adapter.getmData().get(position);

                    adapter.removeItem(position);

                    final boolean[] isDeleted = {true};

                    Snackbar snackbar = Snackbar.make(viewHolder.itemView, "Data anda berhasil dihapus.", Snackbar.LENGTH_LONG);
                    snackbar.setAction("BATAL", v -> {
                        isDeleted[0] = false;

                        adapter.restoreItem(saldo, position);

                        int sum = 0;
                        for (Saldo item : adapter.getmData()){
                            sum += Integer.parseInt(item.getSaldo());
                        }

                        textSum.setText(methodFunction.currencyIdr(sum));

                        sqliteHelper.addPemasukan(saldo);
                    });

                    snackbar.show();

                    if (isDeleted[0]){
                        sqliteHelper.deletePengeluaran(saldo.getDataID());

                        int sum = 0;
                        for (Saldo item : adapter.getmData()){
                            sum += Integer.parseInt(item.getSaldo());
                        }

                        textSum.setText(methodFunction.currencyIdr(sum));
                    }
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
            itemTouchHelper.attachToRecyclerView(rvPengeluaran);

            int sum = 0;
            for (Saldo saldo : saldos) {
                sum += Integer.parseInt(saldo.getSaldo());
            }
            textSum.setText(methodFunction.currencyIdr(sum));
        } else {
            textNotFound.setVisibility(View.VISIBLE);
            rvPengeluaran.setVisibility(View.GONE);
        }
        addButton.setOnClickListener(v -> {
            v.getContext().startActivity(new Intent(v.getContext(), AddPengeluaranActivity.class));
        });
    }
}