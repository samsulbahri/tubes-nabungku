package com.example.mytubes.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.mytubes.entity.Saldo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SqliteHelper extends SQLiteOpenHelper {
//    Nama Database
    public static final String DATABASE_NAME = "nabung.db";

// Versi Database
    public static final int DATABASE_VERSION = 1;

//    Nama Tabel Pemasukan
    public static final String TABLE_PEMASUKAN_NAME = "pemasukan";

//    Nama Tabel Pengeluaran
    public static final String TABLE_PENGELUARAN_NAME = "pengeluaran";

//    Kolom ID pengeluaran dan pemasukan
    public static final String KEY_ID = "id";

    //    Kolom catatan pengeluaran dan pemasukan
    public static final String KEY_CATATAN = "catatan";

    //    Kolom tanggal pengeluaran dan pemasukan
    public static final String KEY_TANGGAL = "tanggal";

    //    Kolom saldo pengeluaran dan pemasukan
    public static final String KEY_SALDO = "saldo";

//    Query untuk membuat tabel pemasukan
    public static final String SQL_TABLE_PEMASUKAN = " CREATE TABLE " + TABLE_PEMASUKAN_NAME
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_SALDO + " INTEGER, "
            + KEY_CATATAN + " TEXT, "
            + KEY_TANGGAL + " TEXT "
            + " ) ";

//    Query untuk membuat tabel pengeluaran
    public static final String SQL_TABLE_PENGELUARAN = " CREATE TABLE " + TABLE_PENGELUARAN_NAME
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_SALDO + " INTEGER, "
            + KEY_CATATAN + " TEXT, "
            + KEY_TANGGAL + " TEXT "
            + " ) ";

//    Constructor
    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

//    Method
//    Membuat Tabel
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_TABLE_PEMASUKAN);
        db.execSQL(SQL_TABLE_PENGELUARAN);
    }

//    Mengupdate tabel ketika database di upgrade
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_PEMASUKAN_NAME);
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_PENGELUARAN_NAME);
        onCreate(db);
    }

//    Mengambil semua data di tabel pemasukan
    public List<Saldo> getAllPemasukan() {
        List<Saldo> lists = new ArrayList<>();

        String QUERY_GET = String.format("SELECT * FROM %s ORDER BY %s", TABLE_PEMASUKAN_NAME, KEY_TANGGAL);


        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery(QUERY_GET, null);

        try {
            if(cursor.moveToFirst()) {
                do {
                    Saldo saldo = new Saldo();

                    saldo.setDataID(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                    saldo.setCatatan(cursor.getString(cursor.getColumnIndex(KEY_CATATAN)));
                    saldo.setSaldo(cursor.getString(cursor.getColumnIndex(KEY_SALDO)));
                    saldo.setTanggal(cursor.getString(cursor.getColumnIndex(KEY_TANGGAL)));

                    lists.add(saldo);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("ERR_DATABASE", "Terjadi masalah saat mengambil data pemasukan");
        } finally {
            if(cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return lists;
    }

    //    Mengambil semua data di tabel pengeluaran
    public List<Saldo> getAllPengeluaran() {
        List<Saldo> lists = new ArrayList<>();

        String QUERY_GET = String.format("SELECT * FROM %s ORDER BY %s", TABLE_PENGELUARAN_NAME, KEY_TANGGAL);

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery(QUERY_GET, null);

        try {
            if(cursor.moveToFirst()) {
                do {
                    Saldo saldo = new Saldo();

                    saldo.setDataID(cursor.getString(cursor.getColumnIndex(KEY_ID)));
                    saldo.setCatatan(cursor.getString(cursor.getColumnIndex(KEY_CATATAN)));
                    saldo.setSaldo(cursor.getString(cursor.getColumnIndex(KEY_SALDO)));
                    saldo.setTanggal(cursor.getString(cursor.getColumnIndex(KEY_TANGGAL)));

                    lists.add(saldo);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("ERR_DATABASE", "Terjadi masalah saat mengambil data pengeluaran");
        } finally {
            if(cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return lists;
    }

//    Mengambil jumlah saldo di tabel pemasukan
    public int getSumPemasukan() {
        int sum = 0;
        List<Saldo> saldos = getAllPemasukan();

        for (Saldo saldo : saldos) {
            sum += Integer.parseInt(saldo.getSaldo());
        }

        return sum;
    }

//    Mengambil jumlah saldo di tabel pengeluaran
    public int getSumPengeluaran() {
        int sum = 0;
        List<Saldo> saldos = getAllPengeluaran();

        for(Saldo saldo : saldos) {
            sum += Integer.parseInt(saldo.getSaldo());
        }

        return sum;
    }

//    Mengambil jumlah uang bersih
    public int getTotalUang() {
        return getSumPemasukan() - getSumPengeluaran();
    }

//    Menambah data ke dalam tabel pemasukan
    public boolean addPemasukan(Saldo saldo) {
        boolean isInsert = false;
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL(String.format(Locale.getDefault(), "INSERT INTO %s (%s,%s,%s) VALUES ( '%s', '%s', %d );", TABLE_PEMASUKAN_NAME, KEY_CATATAN, KEY_TANGGAL, KEY_SALDO, saldo.getCatatan(), saldo.getTanggal(), Integer.valueOf(saldo.getSaldo())));
            isInsert = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isInsert;
    }

    //    Menambah data ke dalam tabel pengeluaran
    public boolean addPengeluaran(Saldo saldo) {
        boolean isInsert = false;
        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL(String.format(Locale.getDefault(), "INSERT INTO %s (%s,%s,%s) VALUES ( '%s', '%s', %d );", TABLE_PENGELUARAN_NAME, KEY_CATATAN, KEY_TANGGAL, KEY_SALDO, saldo.getCatatan(), saldo.getTanggal(), Integer.valueOf(saldo.getSaldo())));
            isInsert = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isInsert;
    }

//    Menghapus data pada tabel pemasukan
    public boolean deletePemasukan(String id) {
        SQLiteDatabase db = getWritableDatabase();

        return db.delete(TABLE_PEMASUKAN_NAME, KEY_ID + "=" + id, null) > 0;
    }

//    Menghapus data pada tabel pengeluaran
    public boolean deletePengeluaran(String id) {
        SQLiteDatabase db = getWritableDatabase();

        return db.delete(TABLE_PENGELUARAN_NAME, KEY_ID + "=" + id, null) > 0;
    }
}
