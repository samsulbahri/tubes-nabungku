package com.example.mytubes.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Saldo implements Parcelable {
    private String catatan, tanggal, saldo, dataID;

    public Saldo(String catatan, String tanggal, String saldo, String dataID) {
        this.catatan = catatan;
        this.tanggal = tanggal;
        this.saldo = saldo;
        this.dataID = dataID;
    }

    public Saldo() {
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getDataID() {
        return dataID;
    }

    public void setDataID(String dataID) {
        this.dataID = dataID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.catatan);
        dest.writeString(this.tanggal);
        dest.writeString(this.saldo);
        dest.writeString(this.dataID);
    }

    protected Saldo(Parcel in) {
        this.catatan = in.readString();
        this.tanggal = in.readString();
        this.saldo = in.readString();
        this.dataID = in.readString();
    }

    public static final Parcelable.Creator<Saldo> CREATOR = new Parcelable.Creator<Saldo>() {

        @Override
        public Saldo createFromParcel(Parcel source) {
            return new Saldo(source);
        }

        @Override
        public Saldo[] newArray(int size) {
            return new Saldo[size];
        }
    };
}

