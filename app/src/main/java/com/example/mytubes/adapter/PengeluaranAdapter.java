package com.example.mytubes.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mytubes.R;
import com.example.mytubes.entity.Saldo;
import com.example.mytubes.helper.MethodFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PengeluaranAdapter extends RecyclerView.Adapter<PengeluaranAdapter.PengeluaranHolder> {

    private List<Saldo> mData;

    MethodFunction methodFunction = new MethodFunction();

    public PengeluaranAdapter(List<Saldo> data){
        mData = data;
    }

    @NonNull
    @Override
    public PengeluaranHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_pemasukan, parent, false);
        return new PengeluaranHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PengeluaranAdapter.PengeluaranHolder holder, int position) {
        Saldo pengeluaran = mData.get(position);

        holder.mCatatanPengeluaran.setText(pengeluaran.getCatatan());
        holder.mSaldoPengeluaran.setText(methodFunction.currencyIdr(Integer.parseInt(pengeluaran.getSaldo())));
        try{
            SimpleDateFormat dateFormatFrom = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            SimpleDateFormat dateFormat = new SimpleDateFormat("d MM", Locale.getDefault());
            Date date = dateFormatFrom.parse(pengeluaran.getTanggal());
            holder.mTanggalPengeluaran.setText(dateFormat.format(date));
        } catch (ParseException a){
            a.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public List<Saldo> getmData() {
        return mData;
    }

    public void removeItem(int position){
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Saldo saldo, int position){
        mData.add(position, saldo);
        notifyItemInserted(position);
    }

    class PengeluaranHolder extends RecyclerView.ViewHolder {
        private TextView mCatatanPengeluaran, mSaldoPengeluaran, mTanggalPengeluaran;
        private CardView cardView;

        public PengeluaranHolder(@NonNull View view){
            super(view);

            cardView = (view).findViewById(R.id.textView23);
            mCatatanPengeluaran = (view).findViewById(R.id.tv_catatan_pemasukan);
            mSaldoPengeluaran = (view).findViewById(R.id.tv_saldo_pemasukan);
            mTanggalPengeluaran = (view).findViewById(R.id.tv_tanggal_pengeluaran);

            view.setFocusable(true);
        }
    }


}
