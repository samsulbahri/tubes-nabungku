package com.example.mytubes.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mytubes.R;
import com.example.mytubes.entity.Saldo;
import com.example.mytubes.helper.MethodFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PemasukanAdapter extends RecyclerView.Adapter<PemasukanAdapter.PemasukanHolder> {

    private List<Saldo> mData;

    MethodFunction methodFunction = new MethodFunction();

    public PemasukanAdapter(List<Saldo> data){
        mData = data;
    }

    @NonNull
    @Override
    public PemasukanHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view =inflater.inflate(R.layout.list_pemasukan, parent, false);
        return new PemasukanHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PemasukanHolder holder, int position) {
        Saldo pemasukan = mData.get(position);

//        holder.cardView.setAnimation(AnimationUtils.loadAnimation(mContext,R.anim.fade_scale_animation));
        holder.mCatatanPemasukan.setText(pemasukan.getCatatan());
        holder.mSaldoPemasukan.setText(methodFunction.currencyIdr(Integer.parseInt(pemasukan.getSaldo())));
        try {
            SimpleDateFormat dateFormatFrom = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM", Locale.getDefault());
            Date date = dateFormatFrom.parse(pemasukan.getTanggal());
            holder.mTanggalPemasukan.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        holder.cardView.setOnClickListener(v -> {
//            Intent updateIntent = new Intent(holder.itemView.getContext(), EditPemasukanActivity.class);
//            updateIntent.putExtra("EXTRA_SALDO", pemasukan);
//            holder.itemView.getContext().startActivity(updateIntent);
//        });
//        holder.itemView.setSelected(mSelectedId.contains(mDataId.get(position)));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class PemasukanHolder extends RecyclerView.ViewHolder {
        private TextView mCatatanPemasukan,mSaldoPemasukan,mTotalPemasukan,mTanggalPemasukan;
        private CardView cardView;

        public PemasukanHolder(@NonNull View itemView){
            super(itemView);

            cardView = (itemView).findViewById(R.id.textView23);
            mCatatanPemasukan = (itemView).findViewById(R.id.tv_catatan_pemasukan);
            mSaldoPemasukan = (itemView).findViewById(R.id.tv_saldo_pemasukan);
//            mTotalPemasukan = (itemView).findViewById(R.id.tv_total_pemasukan);
            mTanggalPemasukan = (itemView).findViewById(R.id.tv_tanggal_pengeluaran);

            itemView.setFocusable(true);
        }
    }

    public List<Saldo> getData() {
        return mData;
    }

    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Saldo saldo, int position) {
        mData.add(position, saldo);
        notifyItemInserted(position);
    }
}
